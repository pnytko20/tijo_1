package com.company;

abstract class Figure {
    public void print(){
    }
}

public class Main {

    public static void printFigure(Figure figure) {
        figure.print();
    }

    public static void main(String[] args) {
        Girl monicaBellucci = new Girl();

        monicaBellucci.receiveFlower(new Rose());
        monicaBellucci.getFlower();
        monicaBellucci.age();

        Square square = new Square();
        Triangle triangle = new Triangle();
        Tree tree = new Tree();

        printFigure(square);
        printFigure(triangle);
        printFigure(tree);
    }
}
