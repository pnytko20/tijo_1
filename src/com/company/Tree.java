package com.company;

public class Tree extends Figure{

    public void print(){
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5 - i; j++)
                System.out.print(" ");
            for (int k = 0; k < (2 * i + 1); k++)
                System.out.print("O");
            System.out.println();
        }
    }
}
