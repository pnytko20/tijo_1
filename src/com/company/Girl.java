package com.company;

class Girl {
    private Flower flower;
    private short age;

    public Girl(){
        this.age = 33;
    }

    public Flower getFlower() {
        return flower;
    }

    public void receiveFlower(Flower flower) {
        this.flower = flower;
    }

    public short age() {
        return age;
    }
}
