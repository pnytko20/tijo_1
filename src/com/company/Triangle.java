package com.company;

public class Triangle extends Figure{

    public void print() {
        System.out.println(" ");
        for (int i = 1; i < 7; ++i) {
            for (int j = 1; j < i; ++j) {
                System.out.print("O  ");
            }
            System.out.println();
        }
        System.out.println(" ");
    }
}
